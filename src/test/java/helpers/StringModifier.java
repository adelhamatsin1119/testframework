package helpers;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StringModifier {
    public static String getUniqueSting(String string) {
        return string +  new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    }
}
