package helpDesk;

import core.BaseSeleniumTest;
import helpers.TestValues;
import org.junit.Assert;
import org.junit.Test;
import readProperties.ConfigProvider;

import static helpers.StringModifier.getUniqueSting;

public class HelpDeskTest extends BaseSeleniumTest {

    @Test
    public void checkTicket(){
        String title = getUniqueSting(TestValues.TEST_TITLE);
        TicketPage ticketPage =  new ManePage().createTicket(title,TestValues.TEST_BODY,TestValues.TEST_EMAIL)
                .openLoginPage()
                .auth(ConfigProvider.ADMIN_LOGIN,ConfigProvider.ADMIN_PASSWORD)
                .findTicket(title);
        Assert.assertTrue(ticketPage.getTittle().contains(title));
        Assert.assertEquals(ticketPage.getBody(),TestValues.TEST_BODY);
        Assert.assertEquals(ticketPage.getEmail(),TestValues.TEST_EMAIL);
    }
}
