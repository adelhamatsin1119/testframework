package helpDesk;

import core.BaseSeleniumPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import readProperties.ConfigProvider;

public class ManePage extends BaseSeleniumPage {
    //  private final By queueList = By.id("id_queue");  //путь к элементу но не его нахождение
    //private final By queueList2 = By.xpath("//select[@id='id_queue']");   //путь к элементу но не его нахождение
    //private WebElement queueListElement = driver.findElement(queueList);
    //след способ более удобен через аннотацию @FindBy - аннотация которая нам позволяет найти именно элемент на странице и именно производить поиск только тогда когда мы обращаемся к этому элементу
    @FindBy(xpath = "//select[@id='id_queue']")
    private WebElement queueList;

    @FindBy(xpath = "//select[@id='id_queue']//option[@value='1']")
    private WebElement queueValue;

    @FindBy(id = "id_title")
    private WebElement title;

    @FindBy(id = "id_body")
    private WebElement body;

    @FindBy(id = "id_due_date")
    private WebElement dateField;

    @FindBy(xpath = "//table[@class='ui-datepicker-calendar']//a[text()='23']")
    private WebElement dateValue;

    @FindBy(id = "id_submitter_email")
    private WebElement email;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement submitButton;

    @FindBy(id = "userDropdown")
    private WebElement LoginButton;

    public ManePage() {
        driver.get(ConfigProvider.URL);
        PageFactory.initElements(driver,this);
    }
    public ManePage createTicket(String titleValue, String bodyValue, String emailValue) {
        queueList.click();
        queueValue.click();
        title.sendKeys(titleValue);
        body.sendKeys(bodyValue);
        dateField.click();
        dateValue.click();
        email.sendKeys(emailValue);
        submitButton.click();
        return this;
    }

    public LoginPage openLoginPage() {
        LoginButton.click();
        return new LoginPage();
    }
}
