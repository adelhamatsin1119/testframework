package core;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;

/**
 * Базовый класс для инициализации селенида
 */

public class BaseTest {

    /**
     * Инициализация селенида с настройками
     */
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        Configuration.browser = "chrome";
        Configuration.driverManagerEnabled = true;
        Configuration.browserSize = "1920x1080";
        Configuration.headless = false;
    }

    /**
     * Выполнение метода перед каждым запуска теста
     */
    @Before
    public void init(){
        setUp();
    }

    /**
     * Выполнение метода после каждого закрытия теста
     */

    @After
    public void tearDown(){
        Selenide.closeWebDriver();
    }

}
