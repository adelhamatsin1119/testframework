package core;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import java.util.concurrent.TimeUnit;

//класс абстрактный для того чтобы, наследоваться в других тестовых классах.
public abstract class BaseSeleniumTest { //у нас есть БАЗОВЫЙ ТЕСТОВЫЙ КЛАСС, где происходит инициализация
    protected WebDriver driver; // есть WebDriver и мы к его экземпляру присвоили настройки с помощью метода ниже setUp(); и нам нужно чтобы эти настройки продолжали быть актуальными на всех страницах

    @Before
    public void setUp(){
        WebDriverManager.chromedriver().setup();//строка, которая скачает нам хром и сразу же его пропишет во всех путях для того чтобы его в дальнейшем использовать
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        BaseSeleniumPage.setDriver(driver);
  }

  @After
    public void tearDown(){
        driver.close();  //закрывает драйвер
        driver.quit();   //закрывает браузер
  }
}
