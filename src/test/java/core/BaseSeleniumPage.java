package core;
import org.openqa.selenium.WebDriver;
public abstract class BaseSeleniumPage {  // у нас есть класс, который будет ПРИМЕНЕН КО ВСЕМ PAGE КЛАССАМ
    protected static WebDriver driver;
    public static void setDriver(WebDriver webdriver) {
        driver = webdriver;
    }
}
